#!/usr/bin/python3

import os
import glob
import datetime
import time

MAX_DELTA_PAYLOAD = 51
MAX_DELTA_SF = 4
MAX_DELTA_BW = 3
MAX_DELTA_CR = 6
MAX_DELTA_RSSI = 3

# define global variables here
messages = []


# define classes here
class Message:
    payload = []
    sf = ""
    cr = ""
    bw = ""
    pw = ""
    rssi = ""
    occurrences = 1


def get_date_time_normal():
    global date_time_normal
    now = datetime.datetime.now()
    # day_of_week = '0' + '%s' % datetime.datetime.today().weekday()
    # print("Day of week ", day_of_week)  # debug
    year = '%s' % now.year
    # print("Year 1 ", year)  # debug
    # year = year[-2:]
    # print("Year 2 ", year)  # debug
    # year = format(int(year, 10), '#04x')
    # print("Year 3", year)  # debug
    # year = year[-2:]
    # print("Year 4", year)  # debug
    month = '%s' % now.month
    # month = format(int(month, 10), '#04x')
    # month = month[-2:]
    # print("Month ", month)  # debug
    day = '%s' % now.day
    # day = format(int(day, 10), '#04x')
    # day = day[-2:]
    # print("Day ", day)  # debug
    current_hour = time.strftime('%H')
    # current_hour = format(int(current_hour, 10), '#04x')
    # current_hour = current_hour[-2:]
    # print("Current Hour ", current_hour)  # debug
    current_mins = time.strftime('%M')
    # current_mins = format(int(current_mins, 10), '#04x')
    # current_mins = current_mins[-2:]
    # print("Current Mins ", current_mins)  # debug
    current_secs = time.strftime('%S')
    # print("Current Secs", current_secs)

    date_time_normal = year + '-' + month + '-' + day + '_' + current_hour + '-' + current_mins + '-' + current_secs
    # day_date_time = day_of_week + year + month + day + current_hour + current_mins

    ##        print('%i-%i-%i '%(now.day,now.month,now.year))#debug
    ##        print(time.strftime('%H:%M:%S'))#debug
    ##        print('%i'%datetime.datetime.today().weekday())#debug
    ##        print day_date_time #debug

    return date_time_normal


# define functions here
def build_msg_list(file_pointer):
    x = 0
    file_str = file_pointer.read()
    # print(file_str)
    messages.clear()
    while x != -1:
        x = file_str.find("Device set up as", x)  # start at previous found position x + 1 to prevent repetition
        if x != -1:  # every time a message was found (did not return -1)
            temp_msg = Message()
            pl_index = (file_str.find("} (", x) + len("} ("))  # payload index
            if pl_index != -1:
                pl_end = file_str.find(")", pl_index)
                if pl_end - pl_index > MAX_DELTA_PAYLOAD:
                    pl_end = pl_index + MAX_DELTA_PAYLOAD
                temp_msg.payload = file_str[pl_index:pl_end]
                if check_homogeneity(temp_msg.payload):  # remove corrupt payloads
                    cr_index = file_str.find("CR_", x)  # find coderate index from start of current message
                    temp_msg.cr = file_str[cr_index:(cr_index + MAX_DELTA_CR)]
                    if cr_index != -1:
                        bw_index = (file_str.find("Bandwidth = ", x) + len("Bandwidth = "))  # bandwidth index
                        bw_end = file_str.find("kHz", bw_index)
                        if bw_end - bw_index > MAX_DELTA_BW:  # if end finder was corrupt in log file
                            bw_end = bw_index + MAX_DELTA_BW
                        temp_msg.bw = file_str[bw_index:bw_end]
                    # spreading factor index
                    sf_index = (file_str.find("Spreading Factor = ", x) + len("Spreading Factor = "))
                    if sf_index != -1:
                        sf_end = file_str.find("\n", sf_index)
                        if sf_end - sf_index > MAX_DELTA_SF:
                            sf_end = sf_index + MAX_DELTA_SF
                        temp_msg.sf = file_str[sf_index:sf_end]
                    rssi_index = (file_str.find("RSSI = ", x) + len("RSSI = "))  # RSSI if it is there
                    if rssi_index != -1:
                        rssi_end = file_str.find("\n", rssi_index)
                        if rssi_end - rssi_index > MAX_DELTA_RSSI:
                            rssi_end = rssi_index + MAX_DELTA_RSSI
                        temp_msg.rssi = file_str[rssi_index:rssi_end]
                    messages.append(temp_msg)
                    # print(temp_msg.payload, temp_msg.sf, temp_msg.bw, temp_msg.cr,
                    #       temp_msg.occurrences, sep=", ")
            if (x + 1) != len(file_str):
                x += 1  # make sure search starts from next char, if end of file has not been reached


def print_results(output_file_name):
    # iterate through all received messages
    #  compare each message to all messages in known messages
    # if payload matches and configuration matches
    # increment occurrences in known messages object
    known_msgs = []  # known message configuration aka measurement distance points
    known_msgs.clear()
    for i in range(0, len(messages)):
        # find if payload + configuration was already seen
        if len(known_msgs) == 0:
            known_msgs.append(messages[i])
        else:
            for j in range(0, len(known_msgs)):
                if (known_msgs[j].payload == messages[i].payload) & (known_msgs[j].sf == messages[i].sf) & \
                        (known_msgs[j].bw == messages[i].bw) & (known_msgs[j].cr == messages[i].cr):
                    # match from received message with known message
                    known_msgs[j].occurrences += 1
                    break  # stop iterating through known messages
                if j == (len(known_msgs) - 1):
                    #  end of list had been reached therefore messages was not known, add it
                    known_msgs.append(messages[i])
                    break  # stop iterating through known messages

    fp_output = open(output_file_name, "w+")

    # iterate through all known messages
    # print their payload + configuration and occurrences
    print("Payload, Spreading Factor, Bandwidth, Code rate, Occurrences")
    fp_output.write("Payload;Spreading Factor;Bandwidth;Code rate;Occurrences\r\n")
    for i in range(0, len(known_msgs)):
        print(known_msgs[i].payload, known_msgs[i].sf, known_msgs[i].bw, known_msgs[i].cr,
              known_msgs[i].occurrences, sep=", ")
        fp_output.write("%s;%s;%s;%s;%d\r\n" % (known_msgs[i].payload, known_msgs[i].sf, known_msgs[i].bw,
                                                    known_msgs[i].cr, known_msgs[i].occurrences))
    fp_output.close()


def check_homogeneity(payload):
    char_reference = payload[0]
    for count, elem in enumerate(payload):
        if payload[count] != char_reference:
            return False
    return True


# actual program from here

os.chdir("/home/pc/serial_log/python_results")
print("Beacon: ")
fp = open(glob.glob("*_Beacon.*")[0], "r+", errors="ignore")
build_msg_list(fp)
print_results("Beacon_transmit_log.csv")
fp.close()
print("\nNodelog 0")
fp = open(glob.glob("*0.log")[0], "r+", errors="ignore")
build_msg_list(fp)
print_results("Nodelog0_receive_log.csv")
fp.close()
print("\nNodelog 1")
fp = open(glob.glob("*1.log")[0], "r+", errors="ignore")
build_msg_list(fp)
print_results("Nodelog1_receive_log.csv")
fp.close()
print("\nNodelog 2")
fp = open(glob.glob("*2.log")[0], "r+", errors="ignore")
build_msg_list(fp)
print_results("Nodelog2_receive_log.csv")
fp.close()
print("\nNodelog 3")
fp = open(glob.glob("*3.log")[0], "r+", errors="ignore")
build_msg_list(fp)
print_results("Nodelog3_receive_log.csv")
fp.close()
print("\nNodelog 4")
fp = open(glob.glob("*4.log")[0], "r+", errors="ignore")
build_msg_list(fp)
print_results("Nodelog4_receive_log.csv")
fp.close()
print("\nNodelog 5")
fp = open(glob.glob("*5.log")[0], "r+", errors="ignore")
build_msg_list(fp)
print_results("Nodelog5_receive_log.csv")
fp.close()

        # mes_start = x + 3  # start of payload
        # pl_end = str.find(")", pl_start)  # index of ")" is the end of payload
        # cur_pl = str[pl_start:pl_end]
        # pl_length = pl_end - pl_start  # length of payload
        #
        # # if found payload not used before, not in list already
        # if not (cur_pl in pl_list):
        #     print(cur_pl)  # print newly found payload
        #     pl_list.append(cur_pl)  # add said payload to list of known payloads
        #     while y != -1:  # search through whole file for similar payload
        #         y = str.find(cur_pl, y + 1)
        #         if y != -1:
        #             cur_pl_counter += 1
        #     print("Received ", cur_pl_counter, " times\n")
        #     # reset some variables
        #     y = 1
        #     cur_pl_counter = 0
        #
        # temp_counter += 1
        # if (x + 1) != len(str):
        #     x += 1  # make sure search starts from next char, if end of file has not been reached




# while x != -1:
#     x = str.find("} (", x)  # start at previous found position x + 1 to prevent repetition
#     if x != -1:  # everytime a payload was found (did not return -1)
#         pl_start = x + 3  # start of payload
#         pl_end = str.find(")", pl_start)  # index of ")" is the end of payload
#         cur_pl = str[pl_start:pl_end]
#         pl_length = pl_end - pl_start  # length of payload
#
#         # if found payload not used before, not in list already
#         if not (cur_pl in pl_list):
#             print(cur_pl)  # print newly found payload
#             pl_list.append(cur_pl)  # add said payload to list of known payloads
#             while y != -1:  # search through whole file for similar payload
#                 y = str.find(cur_pl, y + 1)
#                 if y != -1:
#                     cur_pl_counter += 1
#             print("Received ", cur_pl_counter, " times\n")
#             # reset some variables
#             y = 1
#             cur_pl_counter = 0
#
#         temp_counter += 1
#         if (x + 1) != len(str):
#             x += 1  # make sure search starts from next char, if end of file has not been reached

# print("heartbeat was found ", hb_counter, " amount of times")


# print();print();
# str2 = "Line1-abcdef \nLine2-abc \nLine4-abcd";
# print(str2.split( ))
# print(str2.split('n', 1 )[0])
# print(str2.partition("Line1")[2].partition("L")[0])

# print("Python is really a great language, isn't it?")
# x = input("something:")

# fo = open("foo.txt", "wb")
# print("Name of the file: ", fo.name)
# print("Closed or not: ", fo.closed)
# print("Opening mode: ", fo.mode)
# fo.close()

# fo = open("foo.txt", "w")
# fo.write("Python is a great language, isn't it\nYeah its great\n")
# print("Closed or not: ", fo.closed)
# fo.close()
# print("Closed or not: ", fo.closed)

# fo = open("foo.txt", "r+")
# str = fo.read(90)
# print("Read string is", str)
# fo.close()

# fo = open("foo.txt", "r+")
# str = fo.read(10)
# print ("Read string is : ", str)
# position = fo.tell()
# print("Position is : ", position)
# position = fo.seek(0, 0)
# str = fo.read(10)
# print("again read string is : ", str)
# fo.close()

# #os.mkdir("new_folder")
# os.chdir("/home/pc/PycharmProjects/lora/new_folder")
# print(os.getcwd())
