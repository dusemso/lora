#!/usr/bin/python3

import os
import glob
import datetime
import time
import string


def get_date_time_normal():
    now = datetime.datetime.now()
    # day_of_week = '0' + '%s' % datetime.datetime.today().weekday()
    # print("Day of week ", day_of_week)  # debug
    year = '%s' % now.year
    # print("Year 1 ", year)  # debug
    # year = year[-2:]
    # print("Year 2 ", year)  # debug
    # year = format(int(year, 10), '#04x')
    # print("Year 3", year)  # debug
    # year = year[-2:]
    # print("Year 4", year)  # debug
    month = '%s' % now.month
    # month = format(int(month, 10), '#04x')
    # month = month[-2:]
    # print("Month ", month)  # debug
    day = '%s' % now.day
    # day = format(int(day, 10), '#04x')
    # day = day[-2:]
    # print("Day ", day)  # debug
    current_hour = time.strftime('%H')
    # current_hour = format(int(current_hour, 10), '#04x')
    # current_hour = current_hour[-2:]
    # print("Current Hour ", current_hour)  # debug
    current_mins = time.strftime('%M')
    # current_mins = format(int(current_mins, 10), '#04x')
    # current_mins = current_mins[-2:]
    # print("Current Mins ", current_mins)  # debug
    current_secs = time.strftime('%S')
    # print("Current Secs", current_secs)

    date_time_normal = year + '-' + month + '-' + day + '_' + current_hour + '-' + current_mins + '-' + current_secs
    # day_date_time = day_of_week + year + month + day + current_hour + current_mins
    return date_time_normal


def check_homogeneity(payload):
    char_reference = payload[0]
    for count, elem in enumerate(payload):
        if payload[count] != char_reference:
            return False
    return True


# actual program from here

# os.chdir("/home/pc/Dropbox (Firmwave)/FirmwaveProjects/diamond/Hardware/LoRa/EdgeExpansion/logs/gd_logs")
os.chdir("/home/pc/serial_log/python_results")
print("Glen Dimplex Gateway (ACM0): ")
fp_gateway = open(glob.glob("*GD_Gateway.log")[0], "r+", errors="ignore")
str_gateway = fp_gateway.read()
fp_gateway.close()
# fp = open(get_date_time_normal() + "_GD_Gateway_filtered.csv", "w+")
# fp.write("System Time;Payload\n")
# messages = []
# payload_len = 0
fp_node = open(glob.glob("*GD_node_log.csv")[0], "r+", errors="ignore")
str_node = fp_node.read()
str_node = str_node.upper()  # force uppercase (so we compare apples with apples)
fp_node.close()
fp_drops = open("GD_Drops.csv", "w+")  # file with dropped packages and absolute time
fp_drops.write("System Time;Dropped Payload\n")
fp_forgiving_drops = open("GD_Forgiving_Drops.csv", "w+")  # file with dropped packages and absolute time
fp_forgiving_drops.write("System Time;Dropped Payload\n")

x = 0
while x != -1:
    x = str_gateway.find("[Debug][MRF89XA] Sending message (length: 0x15):", x)
    if x != -1:
        # read time
        time_index = x - 9
        sys_time = str_gateway[time_index:time_index + 8]
        # print("Time is : " + sys_time)
        # read payload
        payload_index = x + len("[Debug][MRF89XA] Sending message (length: 0x15):")
        payload = str_gateway[payload_index:payload_index+72]
        # filter payload for letters only
        payload_list = payload.split()
        temp_list = []
        temp_list.clear()
        # print("pre payload is : " + payload)
        for count, elem in enumerate(payload):  # build temporary list to make current string (removing non hex chars)
            if elem in string.hexdigits:
                temp_list.append(elem)
        payload = ''.join(temp_list)
        payload = payload.upper()  # force all letters to uppercase
        # replace the characters that are random numbers on gateway side but c8 00 00 00 00 on node side
        payload = payload[0:len("770044140001abcdef123456")] + "C800000000" + payload[len("770043140001abcdef123456c800000000"):len("770043140001abcdef123456c800000000675d3c70")]
        payload = "15" + payload  # add 15 to payload, as this is always present on the node side
        # print("Gateway Payload is : " + payload)

        if (";" + payload + "\n") not in str_node:  # gateway has transmitted message and was not received by node. log this drop
            fp_drops.write(sys_time + ';' + payload + "\n")
            print("Harsh packet drops on " + sys_time + " being packet " + payload)

        if payload not in str_node:
            fp_forgiving_drops(sys_time + ';' + payload + '\n')
            print("\t\t\t Forgiving packet drops on " + sys_time + " being packet " + payload)

        # payload_len = len("15" + payload)
        if (x + 1) != len(str_gateway):
            x += 1  # make sure search starts from next char, if end of file has not been reached

fp_drops.close()
fp_forgiving_drops.close()